
function VVConsoleException(message) {
    this.name = 'VVConsoleBridgeException';
    this.message = message;
}

function VVConsole(vvSiteUrl, applicationId, queryParams) {
    this.vvSiteUrl = vvSiteUrl;
    this.applicationId = applicationId;
    this.queryParams = queryParams;
}

VVConsole.init = function () {
    let queryParams = new URLSearchParams(document.location.search.substring(1));

    let vvSiteUrl = queryParams.get('vv_site_url');
    let accessToken = queryParams.get('access_token') || '';
    let applicationId = queryParams.get('application_id') || '_';
    
    if(vvSiteUrl) {
        sessionStorage.setItem('vv_site_url', vvSiteUrl);
    }
    sessionStorage.setItem('application_id', applicationId);
    let vvConsole = VVConsole.bootstrapFromSessionStorage();

    if(accessToken) {
        let authorizationDetails = {
            "access_token": accessToken
        }
        vvConsole.persistAuthorizationDetails(authorizationDetails);
    }
    vvConsole.subscribeToAuthorizationChangeMessage();
    vvConsole.requestAuthorization('syncing');
    vvConsole.requestApplicationDetails();
    return vvConsole;
}

VVConsole.bootstrapFromSessionStorage = function bootstrapFromSessionStorage() {
    let queryParams = new URLSearchParams(document.location.search.substring(1));
    let vvSiteUrl = sessionStorage.getItem('vv_site_url');
    if (vvSiteUrl && vvSiteUrl.endsWith('/')) {
        vvSiteUrl = vvSiteUrl.slice(0, -1);
    }
    let applicationId = sessionStorage.getItem('application_id');
    return new VVConsole(vvSiteUrl, applicationId, queryParams);
}


VVConsole.prototype = {
    vvcAuthKey: 'vvconsole_authorizations',
    applicationsDetailsKey : 'applications_details',
    vvSiteUrlKey: 'vv_site_url',
    applicationIdKey: 'application_id',

    authorizationRequestMessageType : 'authorization_request',
    authorizationChangeMessageType : 'authorization_change',

    applicationsRequestMessageType : 'applications_request',
    applicationsChangeMessageType : 'applications_change',

    retrieveAuthorizationDetails() {
        let persistedString = sessionStorage.getItem(this.vvcAuthKey) || '{}';
        let persistedObj = JSON.parse(persistedString);
        let siteAuthorizationDetails = persistedObj[this.vvSiteUrl];
        return siteAuthorizationDetails || null;
    },

    retrieveOrRequestAuthorizationDetails() {
        let retrievedAuthorizationDetails = this.retrieveAuthorizationDetails();
        if(!retrievedAuthorizationDetails) {
            this.requestAuthorization();
        }
        return retrievedAuthorizationDetails;
    },

    getAuthorizationHeaders() {
        let authorizationDetails = this.retrieveOrRequestAuthorizationDetails();
        if(!authorizationDetails) {
            return null;
        }
        let accessToken = authorizationDetails['access_token'];
        return {
            "Authorization": `Bearer ${accessToken}`
        };
    },

    persistAuthorizationDetails(authorizationDetails) {
        if (authorizationDetails) {
            let persistedString = sessionStorage.getItem(this.vvcAuthKey) || '{}';
            let persistedObj = JSON.parse(persistedString);
            persistedObj[this.vvSiteUrl] = authorizationDetails;
            sessionStorage.setItem(this.vvcAuthKey, JSON.stringify(persistedObj));
            return true;
        }
        return false;
    },

    purgeAuthorizationDetails() {
        let persistedString = sessionStorage.getItem(this.vvcAuthKey) || '{}';
        let persistedObj = JSON.parse(persistedString);
        let siteAuthorizationDetails = persistedObj[this.vvSiteUrl];
        delete persistedObj[this.vvSiteUrl];
        sessionStorage.setItem(this.vvcAuthKey, JSON.stringify(persistedObj));
        return siteAuthorizationDetails;
    },

    requestAuthorization(cause) {
        console.log('requesting authorization', {cause});
        let message = {
            "type": this.authorizationRequestMessageType,
            "vv_site_url": this.vvSiteUrl,
            "application_id": this.applicationId,
            "status": cause || 'unavailable'  // unavailable, expired, syncing
        }
        window.top.postMessage(message, '*');
    },

    persistApplicationsDetails(applicationsDetails) {
        if (applicationsDetails) {
            let persistedString = sessionStorage.getItem(this.applicationsDetailsKey) || '{}';
            let persistedObj = JSON.parse(persistedString);
            persistedObj[this.vvSiteUrl] = applicationsDetails;
            sessionStorage.setItem(this.applicationsDetailsKey, JSON.stringify(persistedObj));
            return true;
        }
        return false;
    },

    getApplicationDetails(jsonClass, api) {
        let applicationsDetails = JSON.parse(sessionStorage.getItem(this.applicationsDetailsKey)) || {};
        if (!applicationsDetails.hasOwnProperty(this.vvSiteUrl)) {
            return [];
        }
        let siteApplicationsDetails = applicationsDetails[this.vvSiteUrl];
        console.log({siteApplicationsDetails});
        let matchedApplications = [];
        for(let app of siteApplicationsDetails) {
            if(app['handledTypes'] && Array.isArray(app['handledTypes']) && app['handledTypes'].indexOf(jsonClass) >= 0
                && app['apis'] && Array.isArray(app['apis']) && app['apis'].indexOf(api) >= 0
                ) {
                matchedApplications.push(app);
            }
        }
        return matchedApplications;
    },

    updateApplicationDetails(applicationName, urlRoot) {
        let applicationInfo = {
            "jsonClass": "Service",
            "url": urlRoot,
            "name": applicationName
        };
        let applicationsDetails = JSON.parse(sessionStorage.getItem(this.applicationsDetailsKey)) || {};
        let siteApplicationsDetails = applicationsDetails[this.vvSiteUrl] || [];
        applicationsDetails[this.vvSiteUrl] = siteApplicationsDetails;
        siteApplicationsDetails.push(applicationInfo);
        sessionStorage.setItem(this.applicationsDetailsKey, JSON.stringify(applicationsDetails));
        return true;
    },

    getApplicationPreferredUrl(jsonClass, api) {
        let matchedApplications = this.getApplicationDetails(jsonClass, api);
        console.log({matchedApplications});
        if(!matchedApplications.length) {
            return null;
        }
        let firstService = matchedApplications[0];
        return firstService.url;
    },

    requestApplicationDetails() {
        let message = {
            "type": this.applicationsRequestMessageType,
            "vv_site_url": this.vvSiteUrl,
            "application_id": this.applicationId
        }
        window.top.postMessage(message, '*');
    },

    subscribeToAuthorizationChangeMessage(callbackFn) {
        window.addEventListener('message', ({ data }) => {
            console.log('authorization change message recieved from parent', { data });
            if(data.type == this.authorizationChangeMessageType) {
                if(data.authorized && data['authorization_details']) {
                    this.persistAuthorizationDetails(data['authorization_details']);
                    if(callbackFn) {
                        callbackFn(this, true);
                    }
                }
                else {
                    this.purgeAuthorizationDetails();
                    if(callbackFn) {
                        callbackFn(this, False);
                    }
                }
            }
            
        }, false);
    },

    subscribeToApplicationChangeMessage(callbackFn) {
        window.addEventListener('message', ({ data }) => {
            if(data.type != this.applicationsChangeMessageType) {
                return;
            }
            this.persistApplicationsDetails(data.applications);
            if(callbackFn) {
                callbackFn(this);
            }
        }, false)
    }
}
